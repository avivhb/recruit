<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    protected $fillable = ['name','email'];

    public function owner(){
        return $this->belongsTo('App\User','user_id');
    }

    // if the name of the function is matched to the key we dont need to add it (statud --> status_id)
    public function status(){
        return $this->belongsTo('App\Status');
    }

}
